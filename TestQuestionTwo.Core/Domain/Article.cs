﻿using System;

namespace TestQuestionTwo.Core.Domain
{
    public class Article
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public DateTime CreateDate { get; set; }
        public string Image { get; set; }
        public virtual Category Category { get; set; }
    }
}