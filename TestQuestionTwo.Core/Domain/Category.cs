﻿using System.Collections.Generic;

namespace TestQuestionTwo.Core.Domain
{
    public class Category
    {
        public Category()
        {
            Articles = new List<Article>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        private ICollection<Article> Articles { get; set; }

    }
}