﻿using System.Linq;

namespace TestQuestionTwo.Core
{
    public interface IEfRepository<TEntity>
    {

        TEntity GetById(int id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        IQueryable<TEntity> Table { get; }
    }
}