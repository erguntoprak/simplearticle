﻿using System;

namespace TestQuestionTwo.Core.ComplexModel
{
    public class ArticleModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        public DateTime CreateDate { get; set; }
        public string Image { get; set; }
    }
}