﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestQuestionTwo.Business.Users;
using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.WebUI.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Login()
        {
            return View();
        }
        // GET: User
        [HttpPost]
        public ActionResult Login(string username,string password)
        {
            var user = _userService.LoginUser(username, password);
            Session["User"] = user;
            return RedirectToAction("Index","Home");
        }

        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(User user)
        {
            if(user==null)
                throw new ArgumentNullException();
            _userService.InsertUser(user);
            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            Session["User"] = null;

            return RedirectToAction("Index", "Home");

        }
    }
}