﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestQuestionTwo.Business.Articles;

namespace TestQuestionTwo.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IArticleService _articleService;
        public HomeController(IArticleService articleService)
        {
            _articleService = articleService;
        }
        public ActionResult Index()
        {
            var model = _articleService.GetAllArticleModels().OrderByDescending(d=>d.CreateDate).ToList();
            return View(model);
        }
        public ActionResult Detail(int id)
        {
            var model = _articleService.GetArticleModel(id);
            return View(model);
        }
    }
}