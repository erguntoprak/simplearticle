﻿using System;
using System.Data.Entity.Migrations;
using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.Data
{
    public class Configuration: DbMigrationsConfiguration<ArticleDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "TestQuestionTwo.Data.ArticleDbContext";
        }

        protected override void Seed(ArticleDbContext context)
        {
            context.Category.Add(new Category() {Name = ".Net"});
            context.Category.Add(new Category() { Name = "C#" });
            context.SaveChanges();

            for (int i = 0; i < 10; i++)
            {
                context.Article.Add(new Article()
                {
                    CategoryId = 1,
                    CreateDate = DateTime.Now.AddDays(i),
                    FullDescription =
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    ShortDescription =
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ",
                    Image = "lorem.jpeg",
                    Title = "What is Lorem Ipsum?"

                });
            }
            for (int i = 0; i < 10; i++)
            {
                context.Article.Add(new Article()
                {
                    CategoryId = 2,
                    CreateDate = DateTime.Now.AddDays(i),
                    FullDescription =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
                    ShortDescription =
                        "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. ",
                    Image = "lorem2.jpeg",
                    Title = "Where can I get some?"

                });
            }



        }
    }
}