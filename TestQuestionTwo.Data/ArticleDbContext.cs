﻿using System.Data.Entity;
using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.Data
{
    public class ArticleDbContext:DbContext
    {
        public ArticleDbContext()
            : base("name=ArticleDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ArticleDbContext, Configuration>("ArticleDbContext"));

        }
        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Category> Category { get; set; }
    }
}