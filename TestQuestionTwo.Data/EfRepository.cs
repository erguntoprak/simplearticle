﻿using System;
using System.Data.Entity;
using System.Linq;
using TestQuestionTwo.Core;

namespace TestQuestionTwo.Data
{
    public class EfRepository<TEntity>:IEfRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _context;

        public EfRepository(DbContext context)
        {
            _context = context;
        }

        private DbSet<TEntity> _entities;

        public TEntity GetById(int id)
        {
            return Entities.Find(id);
        }
        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
           
                Entities.Remove(entity);
                _context.SaveChanges();       
        }
        public void Insert(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Entities.Add(entity);
            _context.SaveChanges();

        }

        public void Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
           
                Entities.Attach(entity);
                _context.SaveChanges();
           
        }
        public IQueryable<TEntity> Table => Entities;

        protected DbSet<TEntity> Entities => _entities ?? (_entities = _context.Set<TEntity>());
    }
}