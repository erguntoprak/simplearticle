﻿using System.Collections.Generic;
using TestQuestionTwo.Core.ComplexModel;

namespace TestQuestionTwo.Business.Articles
{
    public interface IArticleService
    {
        List<ArticleModel> GetAllArticleModels();
        ArticleModel GetArticleModel(int id);
    }
}