﻿using System.Collections.Generic;
using System.Linq;
using TestQuestionTwo.Core;
using TestQuestionTwo.Core.ComplexModel;
using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.Business.Articles
{
    public class ArticleService:IArticleService
    {
        private readonly IEfRepository<Article> _articleRepository;
        private readonly IEfRepository<Category> _categoryRepository;

        public ArticleService(IEfRepository<Article> articleRepository, IEfRepository<Category> categoryRepository)
        {
            _articleRepository = articleRepository;
            _categoryRepository = categoryRepository;
        }

        public List<ArticleModel> GetAllArticleModels()
        {
            var model = from a in _articleRepository.Table
                join c in _categoryRepository.Table on a.CategoryId equals c.Id
                select new ArticleModel()
                {
                    Id = a.Id,
                    CategoryName = c.Name,
                    CreateDate = a.CreateDate,
                    FullDescription = a.FullDescription,
                    Image = a.Image,
                    ShortDescription = a.ShortDescription,
                    Title = a.Title
                };
            
            return model.ToList();
        }

        public ArticleModel GetArticleModel(int id)
        {
            var model = from a in _articleRepository.Table
                join c in _categoryRepository.Table on a.CategoryId equals c.Id
                where a.Id==id
                select new ArticleModel()
                {
                    Id = a.Id,
                    CategoryName = c.Name,
                    CreateDate = a.CreateDate,
                    FullDescription = a.FullDescription,
                    Image = a.Image,
                    ShortDescription = a.ShortDescription,
                    Title = a.Title
                };
            return model.FirstOrDefault();
        }

    }
}