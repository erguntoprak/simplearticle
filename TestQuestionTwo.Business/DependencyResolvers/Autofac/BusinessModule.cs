﻿using System.Data.Entity;
using System.Reflection;
using Autofac;
using TestQuestionTwo.Core;
using TestQuestionTwo.Data;
using Module = Autofac.Module;

namespace TestQuestionTwo.Business.DependencyResolvers.Autofac
{
    public class BusinessModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(ArticleDbContext)).As(typeof(DbContext)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IEfRepository<>));
            builder.RegisterAssemblyTypes(Assembly.Load("TestQuestionTwo.Business"))
                .Where(d => d.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerLifetimeScope();

        }
    }
}