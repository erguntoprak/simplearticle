﻿using System;
using System.Linq;
using System.Web.Mvc;
using TestQuestionTwo.Core;
using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.Business.Users
{
    public class UserService:IUserService
    {
        private IEfRepository<User> _user;

        public UserService(IEfRepository<User> user)
        {
            _user = user;
        }

        public void InsertUser(User user)
        {
            if(user==null)
                throw new ArgumentNullException();
            _user.Insert(user);
        }

        public User LoginUser(string username, string password)
        {
            if (username == null || password == null)
                return null;
            var user = _user.Table.FirstOrDefault(d => d.Username == username && d.Password == password);
            if (user == null)
                return null;
            return  user;
        }
    }
}