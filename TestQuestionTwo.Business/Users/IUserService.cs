﻿using TestQuestionTwo.Core.Domain;

namespace TestQuestionTwo.Business.Users
{
    public interface IUserService
    {
        void InsertUser(User user);
        User LoginUser(string username, string password);
    }
}